import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css','./bmo-smartfolio.global.css','./bmo-smartfolio.portfolios.css','./core.css','./slick.min.css']
})

export class AppComponent {
  siteTitle = '';

  firstPortfolio = {
    header: "Money you might need to withdraw within 2 years",
    title: "Capital Preservation",
    labels: ["Equities 10%","Fixed Income 90%"],
    datasets: [ {data: [10,90],
    backgroundColor: ['#ff9900','#3366cc'] } ],
    chartFooter: "Generally suitable if you have a low tolerance for risk, and would like to receive income while looking to invest for the intermediate to long term."
  };

  secondPortfolio = {
    header: "Money you might need to withdraw within 3 to 6 years",
    title: "Income",
    labels: ["Equities 30%","Fixed Income 70%"],
    datasets: [{data: [30,70],
    backgroundColor: ['#ff9900','#3366cc']}],
    chartFooter: "Generally suitable if you have a low tolerance for risk, a need for regular income and are investing for the intermediate to long term."
  };

  thirdPortfolio = {
    header: "Money you might need to withdraw within 7 to 10 years",
    title: "Balanced",
    labels: ["Equities 50%","Fixed Income 50%"],
    datasets: [{data: [50,50],
    backgroundColor: ['#ff9900','#3366cc'] } ],
    chartFooter: "Generally suitable if you have a medium tolerance for risk (i.e. you are willing to accept some fluctuations in the market value of your investment), are investing for the intermediate to long term, and are looking for some income and long-term growth."
  };

  fourthPortfolio = {
    header: "Money you might need to withdraw within 10 to 15 years",
    title: "Long Term Growth",
    labels: ["Equities 70%","Fixed Income 30%"],
    datasets: [{data: [70,30],
    backgroundColor: ['#ff9900','#3366cc'] } ],
    chartFooter: "Generally suitable if you have a higher than average tolerance for risk, (i.e. you are willing to accept fluctuations in the market value of your investment), would like some income and are looking to invest for long-term growth."
  };

  fifthPortfolio = {
    header: "Money you might feel comfortable investing for over 15 years",
    title: "Equity Growth",
    labels: ["Equities 90%","Fixed Income 10%"],
    datasets: [{data: [90,10],
    backgroundColor: ['#ff9900','#3366cc'] } ],
    chartFooter: "Generally suitable if you have a higher than average tolerance for risk, (i.e. you are willing to accept fluctuations in the market value of your investment), would like to invest primarily in equities and are looking to invest for the long term."
  };

}
