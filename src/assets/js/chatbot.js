  //var accessToken = "718528cb2c5e46308010ce0921ee22a3", //richard's client token
  //var accessToken = "b634af6693c54b1289f0c5e7054a83b5", //richard's client token
  var accessToken = "118ad06072cc4f72b90304c7e4ac7f7a", // my gavin Bot
    baseUrl = "https://api.api.ai/v1/",
    $speechInput,
    $recBtn,
    recognition,
    messageRecording = "Recording...",
    messageCouldntHear = "I couldn't hear you, could you say that again?",
    messageInternalError = "Oh no, there has been an internal server error",
    messageSorry = "I'm sorry, I don't have the answer to that yet.";

  $(document).ready(function() {
 
    $speechInput = $("#speech");

    $recBtn = $("#rec");
    $speechInput.keypress(function(event) {
      if (event.which == 13) {
        event.preventDefault();
        send();
      }
    });

    $recBtn.on("click", function(event) {
      switchRecognition();
    });

    $(".debug__btn").on("click", function() {
      $(this).next().toggleClass("is-active");
      return false;
    });
  });

  function startRecognition() {
    recognition = new webkitSpeechRecognition();
    recognition.continuous = false;
        recognition.interimResults = false;
    recognition.onstart = function(event) {
      respond(messageRecording);
      updateRec();
    };

    recognition.onresult = function(event) {
      recognition.onend = null;
      
      var text = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
          text += event.results[i][0].transcript;
        }
        setInput(text);
      stopRecognition();
    };

    recognition.onend = function() {
      respond(messageCouldntHear);
      stopRecognition();
    };

    recognition.lang = "en-US";
    recognition.start();
  }

  function stopRecognition() {
    if (recognition) {
      recognition.stop();
      recognition = null;
    }
    updateRec();
  }

  function switchRecognition() {
    if (recognition) {
      stopRecognition();
    } else {
      startRecognition();
    }
  }

  function setInput(text) {
    $speechInput.val(text);
    send();
  }

  function updateRec() {
    $recBtn.text(recognition ? "Stop" : "Speak");
  }

  function send() {
    var text = $speechInput.val();
    $.ajax({
      type: "POST",
      url: baseUrl + "query?v=20150910",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      headers: {
        "Authorization": "Bearer " + accessToken
      },
      data: JSON.stringify({query: text, lang: "en", sessionId: "yaydevdiner"}),
      success: function(data) {
        prepareResponse(data);
      },
      error: function() {
        respond(messageInternalError);
      }
    });
  }

  function prepareResponse(val) {
    var debugJSON = JSON.stringify(val, undefined, 2),
      spokenResponse = val.result.fulfillment.speech;
    respond(spokenResponse);
    debugRespond(debugJSON);
  }

  function debugRespond(val) {
    $("#response").text(val);
  }

  function respond(val) {
    if (val == "") {
      val = messageSorry;
    }

    if (val !== messageRecording) {
      var msg = new SpeechSynthesisUtterance();
      msg.voiceURI = "native";
      msg.text = val;
      msg.lang = "en-US";
      window.speechSynthesis.speak(msg);
    }
    //$("#spokenResponse").addClass("is-active").find(".spoken-response__text").html(val);
    var txt = $("textarea#speech.chat");
    
    txt.val( txt.val() + "\n\nGavin>> " + val +"\n\n");

    $('textarea#speech.chat').scrollTop($('textarea#speech.chat')[0].scrollHeight);
  }